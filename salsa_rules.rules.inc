<?php

/**
 * Implements hook_rules_action_info
 *
 * @return array
 */
function salsa_rules_rules_action_info() {
  $items = array();

  $items['salsa_rules_create_supporter'] = array(
  'label'    => 'Create a Supporter',
  'module'  	=> 'Salsa',
  'arguments'  	=> array(
  	'user'  	=> array(
    'type'  => 'user',
    'label'  => 'User to assign as supporter'
    ),
    ),
  'eval input'  => array(
  	'Title',
  	'First_Name',
  	'MI',
  	'Last_Name',
  	'Suffix',
  	'Email',
  	'Phone',
  	'Cell_Phone',
  	'Work_Phone',
  	'Pager',
  	'Home_Fax',
  	'Work_Fax',
  	'Street',
  	'Street2',
  	'street3',
  	'City',
  	'State',
  	'Zip',
  	'County',
  	'Region',
  	'Country',
  	'Latitude',
  	'Longitude',
  	'Organization',
  	'Department',
  	'Occupation',
  	'Source',
  	'Status',
    ),
    );


    $items['salsa_rules_create_group'] = array(
  'label'    => 'Create a Group',
  'module'  	=> 'Salsa',
  'arguments'  	=> array(
  	'node'  	=> array(
    'type'  => 'node',
    'label'  => 'Node to associate with Group'
    ),
    ),
  'eval input'  => array(
  	'Description',
  	'Group_Name'
  	),
  	);

  	$items['salsa_rules_supporter_group'] = array(
  'label'    => 'Add a Supporter to a Group',
  'module'  	=> 'Salsa',
  'eval input'  => array(
  	'supporter_KEY',
  	'groups_KEY'
  	),
  	);

  	$items['salsa_rules_remove_supporter_group'] = array(
  'label'    => 'Remove a Supporter from a Group',
  'module'  	=> 'Salsa',
  'eval input'  => array(
  	'supporter_KEY',
  	'groups_KEY'
  	),
  	);

  	$items['salsa_rules_supporter_sign_petition'] = array(
  'label'    => 'Supporter signs a petition',
  'module'  	=> 'Salsa',
  'eval input'  => array(
  	'supporter_KEY',
  	'petition_KEY'
  	),
  	);

  	$items['salsa_rules_supporter_unsign_petition'] = array(
  'label'    => 'Supporter unsigns a petition',
  'module'  	=> 'Salsa',
  'eval input'  => array(
  	'supporter_KEY',
  	'petition_KEY'
  	),
  	);

  	$items['salsa_rules_supporter_attend_event'] = array(
  'label'    => 'Supporter attends an event',
  'module'  	=> 'Salsa',
  'eval input'  => array(
  	'supporter_KEY',
  	'event_KEY'
  	),
  	);

  	$items['salsa_rules_supporter_stops_attending_event'] = array(
  'label'    => 'Supporter stops attending',
  'module'  	=> 'Salsa',
  'eval input'  => array(
  	'supporter_KEY',
  	'event_KEY'
  	),
  	);

  	$items['salsa_rules_create_petition'] = array(
  'label'    => 'Create a Petition',
  'module'  	=> 'Salsa',
  'arguments'  	=> array(
  	'node'  	=> array(
    'type'  => 'node',
    'label'  => 'Node to associate with Petition'
    ),
    ),
  'eval input'  => array(
  	'Title',
  	'supporter_KEY',
  	'Petition_Content',
  	'groups_KEYS',
  	'Signature_Goal',
  	'Deadline',
    ),
    );

    $items['salsa_rules_create_event'] = array(
  'label'    => 'Create an Event',
  'module'  	=> 'Salsa',
  'arguments'  	=> array(
  	'node'  	=> array(
    'type'  => 'node',
    'label'  => 'Node to associate with Event'
    ),
    ),
  'eval input'  => array(
  	'Event_Name',
  	'supporter_KEY',
  	'Last_Modified',
  	'groups_KEYS',
  	'Date_Created',
  	'Description',
  	'Address',
  	'City',
  	'State',
  	'Zip',
  	'Start',
  	'End',
    ),
    );

    return $items;
}

/**
 * Settings form for Group
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_create_group_form($settings, &$form) {
  $settings += array(
  'Description',
  'Group_Name'
  );

  $form['settings']['Group_Name'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Group Name'),
  '#default_value'	=> $settings['Group_Name']
  );

  $form['settings']['Description'] = array(
  '#type'    => 'textarea',
  '#title'  	=> t('Description'),
  '#default_value'	=> $settings['Description']
  );
}

/**
 * Settings form to add a supporter to a group
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_supporter_group_form($settings, &$form) {
  $settings += array(
  'supporter_KEY',
  'groups_KEY'
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter Key'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['groups_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Group Key'),
  '#default_value'	=> $settings['groups_KEY'],
  '#required'  	=> true
  );
}

/**
 * Settings form to remove a supporter to from a group
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_remove_supporter_group_form($settings, &$form) {
  $settings += array(
  'supporter_KEY',
  'groups_KEY'
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter Key'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['groups_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Group Key'),
  '#default_value'	=> $settings['groups_KEY'],
  '#required'  	=> true
  );
}

/**
 * Settings form to sign a petition
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_supporter_sign_petition_form($settings, &$form) {
  $settings += array(
  'supporter_KEY',
  'petition_KEY'
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter Key'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['petition_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Petition Key'),
  '#default_value'	=> $settings['petition_KEY'],
  '#required'  	=> true
  );
}

/**
 * Settings form to unsign a petition
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_supporter_unsign_petition_form($settings, &$form) {
  $settings += array(
  'supporter_KEY',
  'petition_KEY'
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter Key'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['petition_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Petition Key'),
  '#default_value'	=> $settings['petition_KEY'],
  '#required'  	=> true
  );
}

/**
 * Settings form to attend an event
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_supporter_attend_event_form($settings, &$form) {
  $settings += array(
  'supporter_KEY',
  'event_KEY'
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter Key'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['event_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Event Key'),
  '#default_value'	=> $settings['event_KEY'],
  '#required'  	=> true
  );
}

/**
 * Settings form to stop attending an event
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_supporter_stops_attending_event_form($settings, &$form) {
  $settings += array(
  'supporter_KEY',
  'event_KEY'
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter Key'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['event_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Event Key'),
  '#default_value'	=> $settings['event_KEY'],
  '#required'  	=> true
  );
}

/**
 * Settings form to add a petition
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_create_petition_form($settings, &$form) {
  $settings += array(
  'Title',
  'supporter_KEY',
  'Petition_Content',
  'groups_KEYS',
  'Signature_Goal',
  'Deadline',
  );

  $form['settings']['Title'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Title'),
  '#default_value'	=> $settings['Title'],
  '#required'  	=> true
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['Petition_Content'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Content'),
  '#default_value'	=> $settings['Petition_Content'],
  );

  $form['settings']['groups_KEYS'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Group(s)'),
  '#default_value'	=> $settings['groups_KEYS'],
  '#required'  	=> true
  );

  $form['settings']['Signature_Goal'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Signature Goal'),
  '#default_value'	=> $settings['Signature_Goal'],
  );

  $form['settings']['Deadline'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Deadline'),
  '#default_value'	=> $settings['Deadline'],
  );

}

/**
 * Settings form to add an event
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_create_event_form($settings, &$form) {
  $settings += array(
  'Event_Name',
  'supporter_KEY',
  'Last_Modified',
  'groups_KEYS',
  'Date_Created',
  'Description',
  'Address',
  'City',
  'State',
  'Zip',
  'Start',
  'End'
  );

  $form['settings']['Event_Name'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Title'),
  '#default_value'	=> $settings['Event_Name'],
  '#required'  	=> true
  );

  $form['settings']['supporter_KEY'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Supporter'),
  '#default_value'	=> $settings['supporter_KEY'],
  '#required'  	=> true
  );

  $form['settings']['Last_Modified'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Last Modified'),
  '#default_value'	=> $settings['Last_Modified'],
  );

  $form['settings']['groups_KEYS'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Group(s)'),
  '#default_value'	=> $settings['groups_KEYS'],
  '#required'  	=> true
  );

  $form['settings']['Date_Created'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Date Created'),
  '#default_value'	=> $settings['Date_Created'],
  );

  $form['settings']['Description'] = array(
  '#type'    => 'textarea',
  '#title'  	=> t('Description'),
  '#default_value'	=> $settings['Description'],
  );

  $form['settings']['Address'] = array(
  '#type'    => 'textarea',
  '#title'  	=> t('Address'),
  '#default_value'	=> $settings['Address'],
  );

  $form['settings']['City'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('City'),
  '#default_value'	=> $settings['City'],
  );

  $form['settings']['State'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('State'),
  '#default_value'	=> $settings['State'],
  );

  $form['settings']['Zip'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Zip'),
  '#default_value'	=> $settings['Zip'],
  );

  $form['settings']['Start'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Start Date'),
  '#default_value'	=> $settings['Start'],
  );

  $form['settings']['End'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('End Date'),
  '#default_value'	=> $settings['End'],
  );
}

/**
 * Settings form for Supporter
 *
 * @param array $settings
 * @param array $form
 * @return mixed
 */
function salsa_rules_create_supporter_form($settings, &$form) {
  $settings += array(
  'Title'    => '',
  'First_Name'  => '',
  'MI'    => '',
  'Last_Name'  	=> '',
  'Suffix'  	=> '',
  'Email'    => '',
  'Phone'    => '',
  'Cell_Phone'  => '',
  'Work_Phone'  => '',
  'Pager'    => '',
  'Home_Fax'  	=> '',
  'Work_Fax'  	=> '',
  'street'  	=> '',
  'Street2'  	=> '',
  'Street3'  	=> '',
  'City'    => '',
  'State'    => '',
  'Zip'    => '',
  'County'  	=> '',
  'Region'  	=> '',
  'Country'  	=> '',
  'Latitude'  	=> '',
  'Longitude'  	=> '',
  'Organization'  => '',
  'Department'  => '',
  'Occupation'  => '',
  'Source'  	=> '',
  'Status'  	=> '',
  );

  $form['settings']['Title'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Title'),
  '#default_value'	=> $settings['Title']
  );

  $form['settings']['First_Name'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('First Name'),
  '#default_value'	=> $settings['First_Name']
  );

  $form['settings']['MI'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Middle Initial'),
  '#default_value'	=> $settings['MI']
  );

  $form['settings']['Last_Name'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Last Name'),
  '#default_value'	=> $settings['Last_Name']
  );

  $form['settings']['Suffix'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Suffix'),
  '#default_value'	=> $settings['Suffix']
  );

  $form['settings']['Email'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Email'),
  '#default_value'	=> $settings['Email']
  );

  $form['settings']['Phone'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Phone'),
  '#default_value'	=> $settings['Phone']
  );

  $form['settings']['Cell_Phone'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Cell Phone'),
  '#default_value'	=> $settings['Cell_Phone']
  );

  $form['settings']['Work_Phone'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Work Phone'),
  '#default_value'	=> $settings['Work_Phone']
  );

  $form['settings']['Pager'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Pager'),
  '#default_value'	=> $settings['Pager']
  );

  $form['settings']['Home_Fax'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Home Fax'),
  '#default_value'	=> $settings['Home_Fax']
  );

  $form['settings']['Work_Fax'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Work Fax'),
  '#default_value'	=> $settings['Work_Fax']
  );

  $form['settings']['Street'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Street'),
  '#default_value'	=> $settings['Street']
  );

  $form['settings']['Street2'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Street 2'),
  '#default_value'	=> $settings['Street2']
  );

  $form['settings']['Street3'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Street 3'),
  '#default_value'	=> $settings['Street3']
  );

  $form['settings']['City'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('City'),
  '#default_value'	=> $settings['City']
  );

  $form['settings']['State'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('State'),
  '#default_value'	=> $settings['State']
  );

  $form['settings']['Zip'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Zip'),
  '#default_value'	=> $settings['Zip']
  );

  $form['settings']['County'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('County'),
  '#default_value'	=> $settings['County']
  );

  $form['settings']['Region'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Region'),
  '#default_value'	=> $settings['Region']
  );

  $form['settings']['Country'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Country'),
  '#default_value'	=> $settings['Country']
  );

  $form['settings']['Latitude'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Latitude'),
  '#default_value'	=> $settings['Latitude']
  );

  $form['settings']['Longitude'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Longitude'),
  '#default_value'	=> $settings['Longitude']
  );

  $form['settings']['Organization'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Organization'),
  '#default_value'	=> $settings['Organization']
  );

  $form['settings']['Department'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Department'),
  '#default_value'	=> $settings['Department']
  );

  $form['settings']['Occupation'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Occupation'),
  '#default_value'	=> $settings['Occupation']
  );

  $form['settings']['Source'] = array(
  '#type'    => 'textfield',
  '#title'  	=> t('Source'),
  '#default_value'	=> $settings['Source']
  );

  $form['settings']['Status'] = array(
  '#type'    => 'radios',
  '#title'  	=> t('Status'),
  '#default_value'	=> $settings['Status'],
  '#options'  	=> array('Active' => 'Active', 'Temporarily Inactive' => 'Temporarily Inactive', 'Invisible' => 'Invisible', 'Inactive' => 'Inactive')
  );
}

/**
 * Action to add a Salsa Group
 *
 * @param stdClass $node
 * @param array $settings
 * @return mixed
 */
function salsa_rules_create_group($node, $settings) {
  salsa_rules_insert_node('groups', $settings, $node);
}

/**
 * Action to create a Salsa Supporter
 *
 * @param stdClass $user
 * @param array $settings
 * @return mixed
 */
function salsa_rules_create_supporter($user, $settings) {
  salsa_rules_insert_supporter($settings, $user);
}

/**
 * Action to add a supporter to a group
 *
 * @param array $settings
 * @return mixed
 */
function salsa_rules_supporter_group($settings) {
  salsa_rules_insert('supporter_groups', $settings);
}

/**
 * Action to remove a supporter from a group
 *
 * @param array $settings
 * @return mixed
 */
function salsa_rules_remove_supporter_group($settings) {
  salsa_rules_delete('supporter_groups', $settings);
}

/**
 * Action to create a Salsa Petition
 *
 * @param stdClass $node
 * @param array $settings
 */
function salsa_rules_create_petition($node, $settings) {
  salsa_rules_insert_node('petition', $settings, $node);
}

/**
 * Action to create a Salsa Event
 *
 * @param stdClass $node
 * @param array $settings
 */
function salsa_rules_create_event($node, $settings) {
  salsa_rules_insert_node('event', $settings, $node);
}

/**
 * Action to add a supporter to a group
 *
 * @param array $settings
 * @return mixed
 */
function salsa_rules_supporter_sign_petition($settings) {
  salsa_rules_insert('supporter_petition', $settings);
}

/**
 * Action to remove a supporter from a group
 *
 * @param array $settings
 * @return mixed
 */
function salsa_rules_supporter_unsign_petition($settings) {
  salsa_rules_delete('supporter_petition', $settings);
}

/**
 * Action to add a supporter to a group
 *
 * @param array $settings
 * @return mixed
 */
function salsa_rules_supporter_attend_event($settings) {
  salsa_rules_insert('supporter_event', $settings);
}

/**
 * Action to remove a supporter from a group
 *
 * @param array $settings
 * @return mixed
 */
function salsa_rules_supporter_stops_attending_event($settings) {
  salsa_rules_delete('supporter_event', $settings);
}

