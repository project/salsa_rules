
Salsa Rules 1.x
=========
The Salsa Rules module provides integration with the Democracy in Action Salsa platform through Rules.  It allows the following 
actions to be performed on Salsa:

1. Create a Supporter
2. Create an Event
3. Create a Group
4. Create a Petition
5. Add a Supporter to a Group (add Supporter_Group)
6. Add a Supporter to an Event (Supporter_Event)
7. Add a Supporter to a Petition (Supporter_Petition)
8. Remove any of the above


Installation
============
1. Install, enable and configure the Salsa API module
2. Install & enable the module.
3. Configure Rules

Contributors
============
- whurleyf1 (William Hurley)
